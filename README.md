# Dragon Power (Easy Hack)

xkas patches for the game Dragon Power that make it easier.

1. Your health is no longer drained as times passes.
2. Your starting health is 255 instead of 100.
3. There's an optional patch for invincibility.

# Verion support

All known dumps of the game are supported which include:

* The Japanese version
* The American version
* The French version (Revision 0)
* The French version (Revision 1)